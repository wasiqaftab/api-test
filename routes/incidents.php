<?php
declare(strict_types=1);

use Laravel\Lumen\Routing\Router;

/*
|--------------------------------------------------------------------------
| Incidents Context Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the incidents context.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/** @var Router $router */

$router->group(['prefix' => 'incidents'], static function() use ($router) {
    $router->post("/", 'IncidentsController@store');
    $router->get("{incidentId}", 'IncidentsController@show');
});
