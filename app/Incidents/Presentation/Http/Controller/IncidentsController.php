<?php

declare(strict_types=1);

namespace Fusion\Incidents\Presentation\Http\Controller;

use Fusion\Common\Application\Query\DispatchesQueries;
use Fusion\Incidents\Application\Command\CreateIncidentCommand;
use Fusion\Incidents\Application\Query\GetIncidentQuery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\Response;

class IncidentsController
{
    use DispatchesQueries;

    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }

    public function store(Request $request): Response
    {
        $commmand = new CreateIncidentCommand(json_decode($request->getContent()));

        $this->commandBus->dispatch($commmand);

        return new JsonResponse([
            'incidentId' => $commmand->getIncidentId()->toString()],
            Response::HTTP_CREATED
        );
    }

    public function show(string $id): Response
    {
        $query = GetIncidentQuery::byId($id);

        $incident = $this->dispatchQuery($query);

        return new JsonResponse((object) [
            'id' => $incident->getId()->toString(),
            'description' => $incident->getDescription(),
            'reportedAt' => $incident->getReportedAt(),
            'category' => (object) [
                'id' => $incident->getCategory()->id,
                'name' => $incident->getCategory()->name,
                'colour' => $incident->getCategory()->colour,
            ],
            'position' => (object) [
                'longitude' => $incident->getPosition()->latitude(),
                'latitude' => $incident->getPosition()->longitude(),
            ]
        ], Response::HTTP_OK);
    }
}
