<?php

namespace Fusion\Incidents\Domain\Entity;

use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;

class Incident
{
    /**
     * @var IncidentId
     */
    private $id;
    /**
     * @var string
     */
    private $description;
    /**
     * @var \DateTimeImmutable
     */
    private $reportedAt;
    /**
     * @var object
     */
    private $category;
    /**
     * @var Position
     */
    private $position;

    public function __construct(IncidentId $id, string $description, \DateTimeImmutable $reportedAt, object $category, $position)
    {
        $this->id = $id;
        $this->description = $description;
        $this->reportedAt = $reportedAt;
        $this->category = $category;
        $this->position = $position;
    }

    public function getId(): IncidentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getReportedAt(): \DateTimeImmutable
    {
        return $this->reportedAt;
    }

    public function getCategory(): object
    {
        return $this->category;
    }

    public function getPosition(): Position
    {
        return $this->position;
    }


}
