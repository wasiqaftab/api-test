<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;
use Ramsey\Uuid\Uuid;

final class IncidentId extends ValueObject
{
    /** @var string */
    protected $idString;

    /**
     * @param string $idString
     */
    protected function __construct(string $idString)
    {
        $this->idString = $idString;
    }

    public static function generate(): self
    {
        try {
            return new self(Uuid::uuid4()->toString());
        } catch (\Exception $e) {
            throw new \RuntimeException("Error generating new uuid in IncidentId");
        }
    }

    public static function fromString(string $idString): self
    {
        Assert::that($idString)->uuid();

        return new self($idString);
    }

    public function __toString(): string
    {
        return $this->idString;
    }
}
