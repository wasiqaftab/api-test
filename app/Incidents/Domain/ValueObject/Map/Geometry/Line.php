<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

final class Line extends ValueObject implements Geometry
{
    /** @var Position[] */
    protected $positions;

    // Setup ----

    /**
     * Build up a line from an array of coordinate pairs
     *
     * @param array $coordinates
     *
     * @return Line
     */
    public static function fromCoordinates(array $coordinates): self
    {
        $positions = array_map(function (array $longLat) {
            return Position::fromCoordinates((float) $longLat[1], (float) $longLat[0]);
        }, $coordinates);

        return new self(...$positions);
    }

    /**
     * Collect a set of positions into a line
     *
     * @param Position ...$positions
     *
     * @return Line
     */
    public static function withPositions(Position ...$positions): Line
    {
        return new self(...$positions);
    }

    /**
     * Form a line from an array of positions
     *
     * @param Position[] $positions
     *
     * @return Line
     */
    public static function fromArray(array $positions): Line
    {
        return new self(...$positions);
    }

    /**
     * Parse a string representation of a line
     *
     * @param string $lineString
     *
     * @return Line
     */
    public static function fromString(string $lineString): Line
    {
        Assert::that($lineString)->regex('/^LINESTRING\s\(.+\)/', 'The line string is not in a valid format');

        preg_match_all('/\([\-\d\.\s\,]+\)/', $lineString, $lineStringMatches);

        $coordinateStrings = explode(", ", rtrim(ltrim($lineStringMatches[0][0], '('), ')'));
        $coordinates       = array_map(function (string $coordinateString) {
            return explode(" ", $coordinateString);
        }, $coordinateStrings);

        return self::fromCoordinates($coordinates);
    }

    /**
     * Line constructor.
     *
     * @param Position[] $positions
     */
    private function __construct(Position ...$positions)
    {
        Assert::that($positions)->minCount(2, "A line must have at least 2 points");

        $this->positions = $positions;
    }

    // Commands ----

    /**
     * @inheritDoc
     */
    public function withAdjustedCoordinates(array $coordinates): Geometry
    {
        return self::fromCoordinates($coordinates);
    }

    /**
     * Get a copy of this line with the positions in the opposite order
     *
     * @return Line
     */
    public function inOppositeDirection(): self
    {
        $positions = array_reverse($this->positions);

        return new self(...$positions);
    }

    // Queries ----

    /**
     * Get all the positions that make this Line
     *
     * @return Position[]
     */
    public function positions(): array
    {
        return $this->positions;
    }

    /**
     * Get the number of positions in this line
     *
     * @return int
     */
    public function positionCount(): int
    {
        return count($this->positions);
    }

    /**
     * Get the first position in the line
     *
     * @return Position
     */
    public function firstPosition(): Position
    {
        return $this->positions[0];
    }

    /**
     * Get the last position in the line
     *
     * @return Position
     */
    public function lastPosition(): Position
    {
        $positionCount = $this->positionCount();

        return $this->positions[$positionCount - 1];
    }

    /**
     * Check if this line forms a ring
     *
     * @return bool
     */
    public function isRing(): bool
    {
        return $this->firstPosition()->equals($this->lastPosition());
    }

    /**
     * @inheritDoc
     */
    public function type(): string
    {
        return "LineString";
    }

    /**
     * @inheritDoc
     */
    public function coordinates(): array
    {
        return array_map(function (Position $position) {
            return $position->coordinates();
        }, $this->positions);
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $thisString = implode(", ", array_map(function (Position $position) {
            return sprintf("%s %s", $position->longitude(), $position->latitude());
        }, $this->positions()));

        return sprintf("LINESTRING (%s)", $thisString);
    }
}
