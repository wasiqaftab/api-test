<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

final class Polygon extends ValueObject implements Shape
{
    public const PATTERN = '\[(?:\[(?:\[-?\d+.\d+,\s?-?\d+.\d+\],?){3,}\],?)+\]';

    /** @var Line[] */
    protected $rings;

    // Setup ----

    /**
     * Build up a polygon from an array of ring coordinate arrays
     *
     * @param array $coordinates
     *
     * @return Polygon
     */
    public static function fromCoordinates(array $coordinates): self
    {
        $rings = array_map(function (array $ringCoordinates) {
            return Line::fromCoordinates($ringCoordinates);
        }, $coordinates);

        return new self(...$rings);
    }

    /**
     * Draw a polygon using an array of lines (rings)
     *
     * @param Line[] $rings
     *
     * @return Polygon
     */
    public static function fromArray(array $rings): self
    {
        return new self(...$rings);
    }

    /**
     * Parse a string representation of a polygon
     *
     * @param string $polygonString
     *
     * @return Polygon
     */
    public static function fromString(string $polygonString): Polygon
    {
        Assert::that($polygonString)->regex('/^POLYGON\s\(.+\)/', 'The polygon string is not in a valid format');

        preg_match_all('/\([\-\d\.\s\,]+\)/', $polygonString, $lineStringMatches);
        $rings = [];

        foreach ($lineStringMatches[0] as $lineString) {
            $coordinateStrings = explode(", ", rtrim(ltrim($lineString, '('), ')'));
            $coordinates       = array_map(function (string $coordinateString) {
                return explode(" ", $coordinateString);
            }, $coordinateStrings);

            $rings[] = Line::fromCoordinates($coordinates);
        }

        return new self(...$rings);
    }

    /**
     * Polygon constructor.
     *
     * @param Line[] $rings
     */
    private function __construct(Line ...$rings)
    {
        Assert::that($rings)->notEmpty("A polygon must have at least one complete ring");

        foreach ($rings as $ring) {
            Assert::that($ring->positionCount())->greaterOrEqualThan(3, "Each polygon ring must consist of at least 3 points");
            Assert::that($ring->isRing())->true("Each ring in a polygon must close");
        }

        $this->rings = $this->ensureWindingOrder(...$rings);
    }

    // Commands ----

    /**
     * @inheritDoc
     */
    public function withAdjustedCoordinates(array $coordinates): Geometry
    {
        return self::fromCoordinates($coordinates);
    }

    // Queries ----

    /**
     * Get a copy of the set of rings that make up this polygon
     *
     * @return array
     */
    public function rings(): array
    {
        return $this->rings;
    }

    /**
     * Get the number of rings in this polygon
     *
     * @return int
     */
    public function ringCount(): int
    {
        return count($this->rings);
    }

    /**
     * Get the total number of positions that make up this polygon
     *
     * @return int
     */
    public function positionCount(): int
    {
        $count = array_reduce($this->rings, function (int $carry, Line $ring) {
            return $carry + ($ring->positionCount() - 1);
        }, 0);

        return (int) $count;
    }

    /**
     * @inheritdoc
     */
    public function area(): float
    {
        /** @var float $totalArea */
        $totalArea = array_reduce($this->rings, function (float $carry, Line $ring) {
            return $carry + $this->ringArea($ring);
        }, 0.0);

        return $totalArea;
    }

    /**
     * Calculate the area, only using the exterior ring. This will always be positive.
     *
     * @return float
     */
    public function exteriorArea(): float
    {
        return $this->ringArea($this->rings[0]);
    }

    /**
     * @inheritDoc
     */
    public function centroid(): Position
    {
        $exteriorRing = $this->rings[0];
        $pointCount   = $exteriorRing->positionCount();
        $points       = $exteriorRing->positions();

        $longitude = 0.00;
        $latitude  = 0.00;
        $area      = $this->exteriorArea();

        foreach ($points as $index => $point) {
            $nextIndex = ($index + 1) % $pointCount;
            $nextPoint = $points[$nextIndex];

            $factor = ($point->longitude() * $nextPoint->latitude()) - ($point->latitude() * $nextPoint->longitude());

            $longitude += ($point->longitude() + $nextPoint->longitude()) * $factor;
            $latitude  += ($point->latitude() + $nextPoint->latitude()) * $factor;
        }

        $longitude /= (6 * $area);
        $latitude  /= (6 * $area);

        return Position::fromCoordinates($latitude, $longitude);
    }

    /**
     * @inheritDoc
     */
    public function type(): string
    {
        return "Polygon";
    }

    /**
     * @inheritDoc
     */
    public function coordinates(): array
    {
        return array_map(function (Line $ring) {
            return $ring->coordinates();
        }, $this->rings);
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        $ringStrings = [];

        foreach ($this->rings as $ring) {
            $thisString = implode(", ", array_map(function (Position $position) {
                return sprintf("%s %s", $position->longitude(), $position->latitude());
            }, $ring->positions()));

            $ringStrings[] = sprintf("(%s)", $thisString);
        }

        return sprintf("POLYGON (%s)", implode(", ", $ringStrings));
    }

    // Internals ----

    /**
     * Ensure that external rings points are in anticlockwise order, and all others are clockwise
     *
     * @param Line ...$rings
     *
     * @return Line[]
     */
    private function ensureWindingOrder(Line ...$rings): array
    {
        return array_map(function (int $index, Line $ring) {

            // External ring is the first one
            if (0 == $index) {
                return $this->ringArea($ring) < 0 ? $ring->inOppositeDirection() : $ring;
            }

            return $this->ringArea($ring) > 0 ? $ring->inOppositeDirection() : $ring;
        }, array_keys($rings), $rings);
    }

    /**
     * Calculate the area of a single ring
     *
     * @param Line $ring
     *
     * @return float
     */
    private function ringArea(Line $ring): float
    {
        $pointCount = $ring->positionCount();
        $points     = $ring->positions();
        $area       = 0.00;

        // Use each of the point pairs to add up the total area
        foreach ($points as $index => $point) {
            $nextIndex = ($index + 1) % $pointCount;
            $nextPoint = $points[$nextIndex];

            $area += ($point->longitude() * $nextPoint->latitude()) - ($point->latitude() * $nextPoint->longitude());
        }

        // Divide by 2 to get the final area and return
        return $area / 2;
    }
}
