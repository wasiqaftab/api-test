<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geography;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

/**
 * Country Code
 *
 * @package Fusion\Incidents\Domain\Model\Map\Geography
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
final class CountryCode extends ValueObject
{
    /** @var string */
    protected $code;

    // Setup ----

    /**
     * Get a country code from a string representation
     *
     * @param string $code
     *
     * @return CountryCode
     */
    public static function fromString(string $code): self
    {
        return new self($code);
    }

    /**
     * CountryCode constructor.
     *
     * @param string $code
     */
    private function __construct(string $code)
    {
        Assert::that($code)->length(3, "Country codes must be exactly 3 characters long");

        $this->code = strtoupper($code);
    }

    // Queries ----

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return $this->code;
    }
}
