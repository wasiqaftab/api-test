<?php

declare(strict_types=1);

namespace Fusion\Incidents\Domain\ValueObject\Map\Geography;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\ValueObject;

/**
 * Continent
 *
 * @package Fusion\Incidents\Domain\Model\Map\Geography
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
final class Continent extends ValueObject
{
    /** @var string */
    protected $code;
    /** @var string */
    protected $name;

    // Setup ----

    /**
     * Get a continent instance from a code and name
     *
     * @param string $code
     * @param string $name
     *
     * @return Continent
     */
    public static function fromParts(string $code, string $name): self
    {
        Assert::that($code)->notEmpty();
        Assert::that($name)->notEmpty();

        return new self($code, $name);
    }

    /**
     * Parse a string representation of a continent
     *
     * @param string $continentString
     *
     * @return Continent
     */
    public static function fromString(string $continentString): Continent
    {
        Assert::that($continentString)
            ->notEmpty($continentString)
            ->regex("/[A-Z]{2}:[A-Za-z ]+/");

        [$code, $name] = explode(':', $continentString);

        return new self($code, $name);
    }

    /**
     * Continent constructor.
     *
     * @param string $code
     * @param string $name
     */
    public function __construct(string $code, string $name)
    {
        $this->code = $code;
        $this->name = $name;
    }

    // Queries ----

    /**
     * @return string
     */
    public function code(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function __toString(): string
    {
        return implode(':', [
            $this->code,
            $this->name
        ]);
    }
}
