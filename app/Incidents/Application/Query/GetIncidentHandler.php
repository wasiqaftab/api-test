<?php

namespace Fusion\Incidents\Application\Query;

use Fusion\Incidents\Domain\Entity\Incident;
use Fusion\Incidents\Presentation\Http\Interfaces\IncidentInterface;
use React\Promise\Deferred;

class GetIncidentHandler
{
    /**
     * @var IncidentInterface
     */
    private $repository;

    public function __construct(IncidentInterface $repository)
    {
        $this->repository = $repository;
    }
    public function __invoke(GetIncidentQuery $query, Deferred $promise): void
    {
        $promise->resolve($this->repository->get($query->getIncidentId()));
    }
}
