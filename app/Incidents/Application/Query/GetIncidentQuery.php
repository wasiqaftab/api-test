<?php

namespace Fusion\Incidents\Application\Query;

use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;

class GetIncidentQuery extends Query
{
    use PayloadTrait;

    public static function byId($id)
    {
        $id = IncidentId::fromString($id);

        return new self(
            compact(
                'id'
            )
        );
    }

    public function getIncidentId()
    {
        return $this->payload['id'];
    }
}
