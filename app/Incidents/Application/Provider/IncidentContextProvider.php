<?php

declare(strict_types=1);

namespace Fusion\Incidents\Application\Provider;

use Fusion\Incidents\Application\Command\CreateIncidentCommand;
use Fusion\Incidents\Application\Command\CreateIncidentHandler;
use Fusion\Incidents\Application\Query\GetIncidentHandler;
use Fusion\Incidents\Application\Query\GetIncidentQuery;
use Fusion\Incidents\Presentation\Http\Interfaces\IncidentInterface;
use Fusion\Incidents\Presentation\Http\Repositories\IncidentRepository;
use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Plugin\Router\CommandRouter;
use Prooph\ServiceBus\Plugin\Router\QueryRouter;
use Prooph\ServiceBus\QueryBus;

class IncidentContextProvider extends ServiceProvider
{
    /**
     * All of the command handlers to be registered
     *
     * @var array
     */
    public $commandHandlers = [
        CreateIncidentCommand::class => CreateIncidentHandler::class,
    ];

    /**
     * All of the query handlers to be registered
     *
     * @var array
     */
    public $queryHandlers = [
        GetIncidentQuery::class => GetIncidentHandler::class,
    ];

    /**
     * Register context-specific services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->registerMessageHandlers();
    }

    /**
     * Register command and query handlers
     */
    private function registerMessageHandlers(): void
    {
        // Register command handlers
        $this->app->extend(CommandBus::class, function (CommandBus $commandBus) {
            (new CommandRouter($this->commandHandlers))->attachToMessageBus($commandBus);

            return $commandBus;
        });

        // Register query handlers
        $this->app->extend(QueryBus::class, function (QueryBus $queryBus) {
            (new QueryRouter($this->queryHandlers))->attachToMessageBus($queryBus);

            return $queryBus;
        });
    }
    /**
     * Register regular bindings and singletons
     */
    private function registerBindings(): void
    {
        // Register standard bindings
        foreach ($this->bindings as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
        $this->app->singleton(IncidentInterface::class, IncidentRepository::class);
    }
}
