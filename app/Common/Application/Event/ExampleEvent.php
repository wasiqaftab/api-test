<?php

declare(strict_types=1);

namespace Fusion\Common\Application\Event;

class ExampleEvent extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
