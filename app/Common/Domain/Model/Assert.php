<?php

declare(strict_types=1);

namespace Fusion\Common\Domain\Model;

use Assert\Assert as BaseAssert;

/**
 * Assert
 *
 * This shields the domain model from misinterpretations or BC breaks inside the Assert library, and makes sure it uses
 * our own extended assertion class
 *
 * @package Fusion\Common\Domain\Model
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
abstract class Assert extends BaseAssert
{
    protected static $assertionClass = Assertion::class;
}
