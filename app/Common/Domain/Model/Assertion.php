<?php

declare(strict_types=1);

namespace Fusion\Common\Domain\Model;

use Assert\Assertion as BaseAssertion;

/**
 * Assertion
 *
 * This shields the domain model from misinterpretations or BC breaks inside the Assert library, and lets us set the
 * exception class to use in the case of a failing assertion
 *
 * @package Fusion\Common\Domain\Model
 * @author  James Drew <james.drew@growthcapitalventures.co.uk>
 */
abstract class Assertion extends BaseAssertion
{
    protected static $exceptionClass = InvariantException::class;
}
